# [Legacy SSE Consistency Patches](https://www.nexusmods.com/skyrimspecialedition/mods/34561)

Legacy SSE Consistency Patches

## Description

This mod contains consistency patches for various mods used by the Legacy SSE modding guide. Please note that this patch set is almost 100% thanks to Darkladylexy and ImRevelation. I just adjusted it for my version of the guide.

## Bugs

If you find an issue, let us know [here](https://gitlab.com/legacy-sse/legacy-sse-consistency-patches/issues)!
